FROM alpine
RUN apk --no-cache add python3 ca-certificates py3-psycopg2 postgresql postgresql-dev gcc make python3-dev linux-headers musl-dev && \
    update-ca-certificates && \
    python3 -m ensurepip && \
    pip3 install --upgrade pip setuptools

ADD requirements.txt /opt/polarbytebot/
RUN pip3 install -r /opt/polarbytebot/requirements.txt

ADD . /opt/polarbytebot
WORKDIR /opt/polarbytebot

CMD ["python3", "main.py"]