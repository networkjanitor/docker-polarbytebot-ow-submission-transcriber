import logging
import configparser
import os
import traceback
import sys
from libpolarbytebot.transcriber.overwatch_forum import overwatch_findall_forum_urls, \
    overwatch_findall_forum_urls_mentions, overwatch_forum_parse
import libpolarbytebot.eastereggs
from libpolarbytebot import polarbytebot

__version__ = '0.3.0'
__owner = 'Xyooz'
__source_link = 'https://gitlab.com/networkjanitor/docker-polarbytebot-ow-submission-transcriber'
__user_agent = f'polarbytebot-ow-submissions-transcriber/{__version__} by /u/{__owner}'
__signature = f'\n' \
              f'\n' \
              f'---\n' \
              f'^(Beep boop. $message$)\n' \
              f'\n' \
              f'^(I am robot. My task in this subreddit is to transcribe the content of submitted forumposts from ' \
              f'battle.net. I also link responses of Blizzard employees to /r/Blizzwatch. Please message /u/{__owner} if you ' \
              f'have any questions, suggestions or concerns.) ' \
              f'[^Source ^Code]({__source_link})'
__subreddits = 'test+overwatch'
__microservice_id = 'ow-subm-tscrb'

__enable_submission_linkposts_transcribing = True
__enable_submission_selftext_transcribing = False
__enable_submission_mentions_transcribing = True


def run():
    path_to_conf = os.path.abspath(os.path.dirname(sys.argv[0]))
    path_to_conf = os.path.join(path_to_conf, 'settings.conf')
    cfg = configparser.ConfigParser()
    cfg.read(path_to_conf)
    pbb = polarbytebot.Polarbytebot(signature=__signature, microservice_id=__microservice_id,
                                    oauth_client_id=cfg.get('oauth2', 'client_id'),
                                    oauth_client_secret=cfg.get('oauth2', 'client_secret'),
                                    oauth_redirect_uri=cfg.get('oauth2', 'redirect_uri'),
                                    oauth_username=cfg.get('oauth2', 'username'), praw_useragent=__user_agent,
                                    oauth_refresh_token=cfg.get('oauth2', 'refresh_token'),
                                    database_system=cfg.get('database', 'system'),
                                    database_username=cfg.get('database', 'username'),
                                    database_password=cfg.get('database', 'password'),
                                    database_host=cfg.get('database', 'host'),
                                    database_dbname=cfg.get('database', 'database'))

    for subm in pbb.reddit.subreddit(__subreddits).stream.submissions():
        if subm.author.name == pbb.username:
            continue

        if subm.is_self and __enable_submission_selftext_transcribing:
            handle(subm, subm.selftext, pbb)
        elif subm.is_self and __enable_submission_mentions_transcribing:
            handle(subm, subm.selftext, pbb, search_mentions=True)

        if not subm.is_self and __enable_submission_linkposts_transcribing:
            handle(subm, subm.url, pbb)


def renew_signature(pbb):
    pbb.signature = __signature.replace('$message$', libpolarbytebot.eastereggs.OverwatchEastereggs().funny_msg())


def handle(submission, content, pbb, search_mentions=False):
    # forum stuff
    if search_mentions:
        forum_urls = overwatch_findall_forum_urls_mentions(content, pbb.mention_regex)
    else:
        forum_urls = overwatch_findall_forum_urls(content)

    for url in forum_urls:
        try:
            transcription = overwatch_forum_parse(url)
        except Exception:
            pbb.add_error(url=url, exception_text=traceback.format_exc())
        else:
            renew_signature(pbb)
            pbb.create_comment(submission.name, transcription)


if __name__ == '__main__':
    run()
