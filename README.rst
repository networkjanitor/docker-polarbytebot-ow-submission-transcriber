docker-polarbytebot-ow-submission-transcriber
=============================================

* all actions to the database and all the actual logic is part of the `polarbytebot library <https://gitlab.com/networkjanitor/libpolarbytebot>`_
* this is a small microservice/dockercontainer which queues new comments into the database
* these comments are in this case transcriptions of overwatch forum posts